<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'Admin',
            'email' => 'admin@sekolahku.com',
            'password' => Hash::make('password'),
            'role' => 'admin'
        ],
        [
            'name' => 'Teacher',
            'email' => 'teacher@sekolahku.com',
            'password' => Hash::make('password'),
            'role' => 'teacher'
        ],
        [
            'name' => 'Student',
            'email' => 'student@sekolahku.com',
            'password' => Hash::make('password'),
            'role' => 'student'
        ]]);
    }
}
