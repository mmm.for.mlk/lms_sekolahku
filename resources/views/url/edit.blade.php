@extends('layouts.master')

@section('title')
    {{ $url->section->course->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>Update {{ $url->name }}</h2>
                    </div>
                </div>
                <div class="full progress_bar_inner">
                    <div class="padding_infor_info">
                        <form action="/mod/url/{{ $url->id }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Judul External Link</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    oninvalid="this.setCustomValidity('Nama External Link harus diisi')"
                                    value="{{ $url->name }}" required>
                                <small id="emailHelp" class="form-text text-muted"></small>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" rows="3">{{ $url->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="url">Url</label>
                                <input type="text" class="form-control" id="url" name="url"
                                    oninvalid="this.setCustomValidity('Url harus diisi')" value="{{ $url->url }}"
                                    required>
                                @error('url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <a href="/course/{{ $course->id }}" class="btn btn-info">Kembali</a>
                            <button type="submit" class="btn btn-primary">Update External Link</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
