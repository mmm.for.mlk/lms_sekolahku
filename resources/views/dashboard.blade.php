@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')
    @if (Auth::user()->role == 'admin')
        <div class="row">
            <div class="col-md-4">
                <div class="full counter_section margin_bottom_30">
                    <div class="couter_icon">
                        <div>
                            <i class="fa fa-user red_color"></i>
                        </div>
                    </div>
                    <div class="counter_no">
                        <div>
                            <p class="total_no">2500</p>
                            <p class="head_couter">Teachers</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="full counter_section margin_bottom_30">
                    <div class="couter_icon">
                        <div>
                            <i class="fa fa-user blue2_color"></i>
                        </div>
                    </div>
                    <div class="counter_no">
                        <div>
                            <p class="total_no">2500</p>
                            <p class="head_couter">Students</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="full counter_section margin_bottom_30">
                    <div class="couter_icon">
                        <div>
                            <i class="fa fa-graduation-cap yellow_color"></i>
                        </div>
                    </div>
                    <div class="counter_no">
                        <div>
                            <p class="total_no">2500</p>
                            <p class="head_couter">Courses</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <!-- tab style 1 -->
            <div class="col-md-12">
                <div class="white_shd full margin_bottom_30">
                    <div class="full graph_head">
                        <div class="heading1 margin_0">
                            <h2>Your Course</h2>
                        </div>
                        @if (Auth::user()->role == 'teacher')
                            <div class="text-right ">
                                <button type="button" class="btn btn-primary m-0" data-toggle="modal"
                                    data-target="#exampleModalCenter">
                                    Tambah Course
                                </button>
                            </div>
                        @endif
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="padding_infor_info">
                                    <div class="row">
                                        @forelse ($enrollments as $item)
                                            <div class="col-md-6 col-lg-4 mb-3">
                                                <div class="card">
                                                    <img class="card-img-top"
                                                        src="{{ asset('images/layout_img/g1.jpg') }}"
                                                        alt="Card image cap">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $item->course->name }}</h5>
                                                        <p class="card-text">{{ $item->course->description }}
                                                        </p>
                                                        <div class="d-flex justify-content-between">
                                                            <a href="{{ url('course/' . $item->course->id) }}"
                                                                class="btn btn-primary">Lihat course</a>
                                                            @if (Auth::user()->role == 'teacher')
                                                                <div class="dropdown_section m-0">
                                                                    <div class="dropdown">
                                                                        <button type="button"
                                                                            class="btn btn-sm btn-outline-secondary"
                                                                            data-toggle="dropdown"><i
                                                                                class="fa fa-ellipsis-h text-dark"
                                                                                aria-hidden="true"></i></button>
                                                                        <div class="dropdown-menu">
                                                                            <form
                                                                                action="{{ url('course/' . $item->course->id) }}"
                                                                                method="post">
                                                                                @csrf
                                                                                @method('delete')
                                                                                <a class="dropdown-item text-primary"
                                                                                    href="{{ url('course/' . $item->course->id . '/edit') }}">Update</a>
                                                                                <button class="dropdown-item text-danger"
                                                                                    type="submit">Delete</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            @if (Auth::user()->role == 'teacher')
                                                <p>Anda belum mengajar di course manapun, Silakan klik Tambah Course untuk
                                                    membuat course baru.</p>
                                            @else
                                                <p>Anda belum mengikuti course manapun. Silakan klik menu courses dan pilih
                                                    course
                                                    yang ingin diikuti.</p>
                                            @endif
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


@endsection
@section('modal')
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Course</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/course" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nama Mata Pelajaran</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Nama mata pelajaran harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="token">Enrollkey</label>
                            <input type="text" class="form-control" id="token" name="token" maxlength="5">
                            <small id="emailHelp" class="form-text text-muted">Enrollkey digunakan sebagai password saat
                                murid ingin mengikuti course ini. Jika tidak diisi, enrollkey akan dibuat secara
                                random</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah Course</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
