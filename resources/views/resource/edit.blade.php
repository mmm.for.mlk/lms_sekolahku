@extends('layouts.master')

@section('title')
    {{ $resource->section->course->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>Update {{ $resource->name }}</h2>
                    </div>
                </div>
                <div class="full progress_bar_inner">
                    <div class="padding_infor_info">
                        <form action="/mod/resource/{{ $resource->id }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Judul Bahan Ajar</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    oninvalid="this.setCustomValidity('Nama mata pelajaran harus diisi')"
                                    value="{{ $resource->name }}" required>
                                <small id="emailHelp" class="form-text text-muted"></small>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" rows="3">{{ $resource->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="attachment">Attachment</label>
                                <input type="file" name="attachment" class="form-control-file" id="attachment">
                            </div>

                            <a href="/course/{{ $course->id }}" class="btn btn-info">Kembali</a>
                            <button type="submit" class="btn btn-primary">Update Bahan Ajar</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
