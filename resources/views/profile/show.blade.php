@extends('layouts.master')

@section('title')
    Profile
@endsection

@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>User profile</h2>
                    </div>
                </div>
                <div class="full price_table padding_infor_info">
                    <div class="row">
                        <!-- user profile section -->
                        <!-- profile image -->
                        <div class="col-lg-12">
                            <div class="full dis_flex center_text">
                                @if ($user->profile != null)
                                    <div class="profile_img"><img width="180" class="rounded-circle"
                                            src="{{ asset('picture/' . $user->profile->picture) }}" alt="#" /></div>
                                @else
                                    <div class="profile_img"><img width="180" class="rounded-circle"
                                            src="{{ asset('images/no-avatar.png') }}" alt="#" /></div>
                                @endif
                                <div class="profile_contant">
                                    <div class="contact_inner">
                                        <h3>{{ $user->name }}</h3>
                                        <p><strong>Role: </strong>{{ ucwords($user->role) }}</p>
                                        @if ($user->profile != null)
                                            <p>{{ $user->profile->bio }} </p>
                                        @endif
                                        <ul class="list-unstyled">
                                            <li><i class="fa fa-envelope-o"></i> : {{ $user->email }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection
