@extends('layouts.master')

@section('title')
    Profile
@endsection

@section('content')
    <div class="row column1">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>Edit Profile</h2>
                    </div>
                </div>
                <div class="full price_table padding_infor_info">
                    <div class="row">
                        <!-- user profile section -->
                        <!-- profile image -->
                        <form action="/profile/{{$profile->id}}" method="post" enctype="multipart/form-data">
                            @csrf
                          @method('put')
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <input type="hidden" name="user_id" value="{{auth::user()->id}}">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}"
                                        >
                                    @error('name')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Email</label>
                                    <input type="text" class="form-control" name="email"
                                        value="{{ Auth::user()->email }}">
                                    @error('email')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Picture</label>
                                    <input type="file" class="form-control" name="picture">
                                    @error('picture')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-12 ">
                                    <label for="name">Bio</label>
                                    <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
                                    @error('bio')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Social Media</label>
                                    <input type="text" class="form-control" name="social_media" value="{{$profile->social_media}}">
                                    @error('social_media')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Edit Profile</button>
                            </div>
                        </form>

                        <!-- profile contant section -->

                        <!-- end user profile section -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection
