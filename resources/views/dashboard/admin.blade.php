@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="full counter_section margin_bottom_30">
                <div class="couter_icon">
                    <div>
                        <i class="fa fa-user red_color"></i>
                    </div>
                </div>
                <div class="counter_no">
                    <div>
                        <p class="total_no">{{ $teacher }}</p>
                        <p class="head_couter">Teachers</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="full counter_section margin_bottom_30">
                <div class="couter_icon">
                    <div>
                        <i class="fa fa-user blue2_color"></i>
                    </div>
                </div>
                <div class="counter_no">
                    <div>
                        <p class="total_no">{{ $student }}</p>
                        <p class="head_couter">Students</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="full counter_section margin_bottom_30">
                <div class="couter_icon">
                    <div>
                        <i class="fa fa-graduation-cap yellow_color"></i>
                    </div>
                </div>
                <div class="counter_no">
                    <div>
                        <p class="total_no">{{ $course }}</p>
                        <p class="head_couter">Courses</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>Tambah User</h2>
                    </div>
                </div>
                <div class="padding_infor_info">
                    <form action="/dashboard" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Nama User harus diisi')" required>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email </label>
                            <input type="text" class="form-control" id="email" name="email"
                                oninvalid="this.setCustomValidity('Email harus diisi')" required>
                            @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password </label>
                            <input type="text" class="form-control" id="password" name="password"
                                oninvalid="this.setCustomValidity('Password harus diisi')" required>
                            @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-control" id="role" name="role" required>
                                <option value="teacher">Teacher</option>
                                <option value="student">Student</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Tambah User</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>Tambah Course</h2>
                    </div>
                </div>
                <div class="padding_infor_info">
                    <form action="/course" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nama Mata Pelajaran</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Nama mata pelajaran harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="token">Enrollkey</label>
                            <input type="text" class="form-control" id="token" name="token">
                            <small id="emailHelp" class="form-text text-muted">Enrollkey digunakan sebagai password saat
                                murid ingin mengikuti course ini. Jika tidak diisi, enrollkey akan dibuat secara
                                random</small>
                        </div>
                        <button type="submit" class="btn btn-primary w-100">Tambah Course</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
