@extends('layouts.master')

@section('title')
    Data Absensi Siswa
@endsection

@section('content')

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama Murid</th>
                <th scope="col">Kehadiran</th>
                <th scope="col">Waktu</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $key => $item)
                <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$item->user->name}} </td>
                    <td> {{$item->status}} </td>
                    <td> {{$item->AttendanceForm->duedate}} </td>
                </tr>
            @empty
                <h1>Belum ada Kehadiran</h1>
            @endforelse
        </tbody>
    </table>

@endsection