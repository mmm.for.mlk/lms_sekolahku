@extends('layouts.master')

@section('title')
    Silahkan buat Materi anda
@endsection

@section('content')
<form action="/section" method="post">
    @csrf
    <div class="form-group">
        <label>Nama Materi</label>
        <input type="text" class="form-control" name="name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Deskripsi Materi</label>
        <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection