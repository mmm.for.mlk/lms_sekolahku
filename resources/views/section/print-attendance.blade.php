<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pdf</title>
</head>

<body>
    <h2>Rekapitulasi Presensi</h2>
    <table border="1" cellpadding="10" cellspacing="0" width="100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama Murid</th>
                <th scope="col">Kehadiran</th>
                <th scope="col">Waktu</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data['attendance_records'] as $key => $item)
                <tr>
                    <td style="text-align: center"> {{ $key + 1 }} </td>
                    <td> {{ $item['user']['name'] }} </td>
                    <td style="text-align: center"> {{ $item['status'] }} </td>
                    <td style="text-align: center"> {{ $item['date'] }} </td>
                </tr>
            @empty
                <h1>Belum ada Kehadiran</h1>
            @endforelse
        </tbody>
    </table>
</body>

</html>
