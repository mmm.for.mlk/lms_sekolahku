@extends('layouts.master')

@section('title')
    {{ $assignment->section->course->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>{{ $assignment->name }}</h2>
                    </div>
                </div>
                @if (date_create('now', timezone_open('Asia/Jakarta')) > $opendate)
                    <div class="full progress_bar_inner">
                        <div class="padding_infor_info">
                            <div class="row">
                                <div class="col">
                                    <p>{{ $assignment->description }}</p>
                                    @if ($assignment->attachment != null)
                                        <span class="name_user"><a
                                                href="{{ asset('resource/attachment/' . $assignment->attachment) }}"
                                                target="_blank"><i class="fa fa-file-pdf-o red_color fa-fw"
                                                    aria-hidden="true"></i>
                                                {{ $assignment->name }}</a></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full graph_head d-flex justify-content-between">
                        <div class="heading1 margin_0">
                            <h2>Submission Status</h2>
                        </div>
                    </div>
                    <div class="full progress_bar_inner">
                        <div class="padding_infor_info">
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Submission Status</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    @if ($assignmentstatus != null)
                                        <p>Submitted</p>
                                    @else
                                        <p>No Attempts</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Grading Status</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    @if ($assignmentstatus != null)
                                        @if (($score = $assignment->collections()->where('user_id', Auth::id())->first()->score) == null)
                                            <p>Not Graded</p>
                                        @else
                                            <p>{{ $score }}</p>
                                        @endif
                                    @else
                                        <p>Not Graded</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Open Date</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <p>{{ $opendate->format('\J\a\m H:i \t\a\n\g\g\a\l d F Y') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Due Date</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <p>{{ $duedate->format('\J\a\m H:i \t\a\n\g\g\a\l d F Y') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Time Remaining</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    @if (date_create('now', timezone_open('Asia/Jakarta')) > $duedate)
                                        <p>Anda terlambat
                                            {{ $interval->days . ' hari ' . $interval->h . ' jam ' . $interval->i . ' menit ' . $interval->s . ' detik' }}
                                        </p>
                                    @else
                                        <p>{{ $interval->days . ' hari ' . $interval->h . ' jam ' . $interval->i . ' menit ' . $interval->s . ' detik' }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col d-flex justify-content-center">
                                    @if ($assignmentstatus != null)
                                        @if ($assignment->collections()->where('user_id', Auth::id())->first()->score == null)
                                            <form action="/collection/{{ Auth::id() }}" method="post"
                                                enctype="multipart/form-data">
                                                @csrf
                                                @method('PUT')

                                                <input type="hidden" name="assignment_id" value="{{ $assignment->id }}">
                                                <div class="form-group">
                                                    <label for="attachment">Upload Tugas</label>
                                                    <input type="file" name="attachment" class="form-control-file"
                                                        id="attachment" required>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary w-100">Kumpulkan tugas</a>
                                                </div>
                                            </form>
                                        @endif
                                    @else
                                        <form action="/collection" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                            <input type="hidden" name="assignment_id" value="{{ $assignment->id }}">
                                            <div class="form-group">
                                                <label for="attachment">Upload Tugas</label>
                                                <input type="file" name="attachment" class="form-control-file"
                                                    id="attachment" required>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary w-100">Kumpulkan tugas</a>
                                            </div>
                                        </form>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="full progress_bar_inner">
                        <div class="padding_infor_info">
                            <p>Tugas ini belum dapat diakses. Anda dapat mengaksesnya setelah
                                {{ $opendate->format('\J\a\m H:i \t\a\n\g\g\a\l d F Y') }}</p>
                            <a href="/course/{{ $course->id }}" class="btn btn-info">Kembali</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
