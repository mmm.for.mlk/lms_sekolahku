@extends('layouts.master')

@section('title')
    {{$course->name . " - " . $course->enrollkey}}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full inner_elements">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab_style1">
                                <div class="tabbar padding_infor_info">
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                                href="#nav-home" role="tab" aria-controls="nav-home"
                                                aria-selected="true">Description</a>
                                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                                href="#nav-profile" role="tab" aria-controls="nav-profile"
                                                aria-selected="false">Pengajar</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                            aria-labelledby="nav-home-tab">
                                            <p>{{$course->description}}
                                            </p>
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                            aria-labelledby="nav-profile-tab">
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                                doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                                veritatis et
                                                quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                                                voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                                consequuntur magni dolores eos
                                                qui ratione voluptatem sequi nesciunt.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @forelse ($section as $item)
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>{{$item->name}}</h2>
                        @if (Auth::user()->role == 'teacher')
                            <form action="{{url('/section/destroy/' . $course->id . '/' . $item->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        @endif
                    </div>
                </div>
                <div class="full progress_bar_inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="msg_list_main">
                                <ul class="msg_list">
                                    {{-- red untuk resources --}}
                                    <li style="border-left-color:red">
                                        <span>
                                            <span class="name_user"><i class="fa fa-file-pdf-o red_color fa-fw"
                                                    aria-hidden="true"></i> Resource Name</span>
                                            <span class="msg_user">Resource description, this section will be hidden
                                                if there are no description for the resource</span>
                                            <span class="time_ago">12 min ago</span>
                                        </span>
                                    </li>
                                    {{-- aqua untuk external_links --}}
                                    <li style="border-left-color:aqua">
                                        <span>
                                            <span class="name_user"><i class="fa fa-external-link blue2_color fa-fw"
                                                    aria-hidden="true"></i> External Link Name</span>
                                            <span class="msg_user">External link description, this section will be
                                                hidden if there are no description for the external link</span>
                                            <span class="time_ago">12 min ago</span>
                                        </span>
                                    </li>
                                    {{-- orange untuk attendance --}}
                                    <li style="border-left-color:orange">
                                        <span>
                                            <span class="name_user"><i
                                                    class="fa fa-calendar-check-o yellow_color fa-fw"
                                                    aria-hidden="true"></i> Attendance</span>
                                            <span class="msg_user">Attendance description, this section will be
                                                hidden if there are no description for the attendance</span>
                                            <span class="time_ago">12 min ago</span>
                                        </span>
                                    </li>
                                    {{-- seagreen untuk assignment --}}
                                    <li style="border-left-color:seagreen">
                                        <span>
                                            <span class="name_user"><i class="fa fa-briefcase green_color fa-fw"
                                                    aria-hidden="true"></i> Assignment/Quiz</span>
                                            <span class="msg_user">Assignment/Quiz description, this section will be
                                                hidden if there are no description for the Assignment/Quiz</span>
                                            <span class="time_ago">12 min ago</span>
                                        </span>
                                    </li>

                                    @if (Auth::user()->role == 'teacher')
                                        <li style="border: none" class="d-flex justify-content-center">
                                            <div class="dropdown_section">
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                                        data-toggle="dropdown"><i class="fa fa-plus-circle fa-fw fa-lg"
                                                            aria-hidden="true"></i> Konten</button>
                                                    <div class="dropdown-menu">
                                                        <button type="button" class="dropdown-item" data-toggle="modal"
                                                            data-target="#bahanAjarModal">Tambah Bahan Ajar</button>
                                                        <button type="button" class="dropdown-item" data-toggle="modal"
                                                            data-target="#externalLinkModal">Tambah External Link</button>
                                                        <button type="button" class="dropdown-item" data-toggle="modal"
                                                            data-target="#attendanceModal">Tambah Attendance</button>
                                                        <button type="button" class="dropdown-item" data-toggle="modal"
                                                            data-target="#assignmentModal">Tambah Assignment</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@empty

@if (Auth::user()->role == 'teacher')
    <div class="card">
        <div class="card-body text-center">
            <h4>Silahkan tambah materi anda</h4>
        </div>
    </div>
@endif

@endforelse
    @if (Auth::user()->role == 'teacher')
        <div class="text-center mb-3">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSection">
                <i class="fa fa-plus-circle fa-fw fa-lg" aria-hidden="true"></i> Section
            </button>
        </div>
    @endif
@endsection


@section('modal')
    <div class="modal fade" id="bahanAjarModal" tabindex="-1" role="dialog" aria-labelledby="bahanAjarModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Bahan Ajar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tambahkan form untuk membuat course di bagian sini dengan action mengarah ke controller Course method
                    store
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah Course</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="externalLinkModal" tabindex="-1" role="dialog" aria-labelledby="externalLinkModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah External Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tambahkan form untuk membuat course di bagian sini dengan action mengarah ke controller Course method
                    store
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah Course</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="attendanceModal" tabindex="-1" role="dialog" aria-labelledby="attendanceModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Attendance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tambahkan form untuk membuat course di bagian sini dengan action mengarah ke controller Course method
                    store
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah Course</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="assignmentModal" tabindex="-1" role="dialog" aria-labelledby="assignmentModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Assignment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tambahkan form untuk membuat course di bagian sini dengan action mengarah ke controller Course method
                    store
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah Course</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalSection" tabindex="-1" role="dialog" aria-labelledby="modalSectionTitle"
        aria-hidden="true">
        <form action="{{url('/section/create/' . $course->id)}}" method="post">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Section</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            @csrf
                            <label for="name">Nama Materi</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary m-0">Tambah Course</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
