@extends('layouts.master')

@section('title')
    {{ $assignment->section->course->name }}
@endsection


@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>{{ $assignment->name }}</h2>
                    </div>
                </div>
                <div class="table_section padding_infor_info">
                    <div class="table-responsive-sm">
                        <table id="attendanceRecords" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Nama Murid</th>
                                    <th scope="col">Tugas</th>
                                    <th scope="col">Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($participant as $item)
                                    @if ($item->user->role == 'student')
                                        <tr>
                                            <td> {{ $key = $key + 1 }} </td>
                                            <td> {{ $item->user->name }} </td>
                                            @if (($data = $item->user->collections()->where('assignment_id', $assignment->id)->first()) != null)
                                                <td><a
                                                        href="{{ asset('resource/collection/' . $data->attachment) }}">{{ $data->attachment }}</a>
                                                </td>
                                                @if ($data->score == null)
                                                    <td>
                                                        <button type="button"
                                                            class="btn btn-outline-primary tampilModalBeriNilai"
                                                            data-toggle="modal" data-target="#beriNilai"
                                                            data-name="{{ $item->user->name }}"
                                                            data-user="{{ $item->user->id }}"
                                                            data-assignment="{{ $assignment->id }}">
                                                            Beri nilai
                                                        </button>
                                                    </td>
                                                @else
                                                    <td>
                                                        {{ $data->score }}
                                                    </td>
                                                @endif
                                            @else
                                                <td> Belum mengumpulkan tugas </td>
                                                <td> - </td>
                                            @endif

                                            <td> {{ $item->date }} </td>
                                        </tr>
                                    @endif
                                @empty
                                    <h1>Belum ada Kehadiran</h1>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="beriNilai" tabindex="-1" role="dialog" aria-labelledby="beriNilaiLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="beriNilaiLabel">Beri Nilai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="beriNilaiForm" action="/collection" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <input type="hidden" name="user_id" id="collectionUserID">
                        <input type="hidden" name="assignment_id" id="collectionAssignmentID">
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" class="form-control" id="collectionName" name="name" disabled>
                        </div>
                        <div class="form-group">
                            <label for="score">Nilai</label>
                            <input type="number" min="0" max="100" class="form-control" id="collectionScore" name="score"
                                required>
                            <small id="emailHelp" class="form-text text-muted">Beri nilai dengan skala 1-100</small>
                            @error('score')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Beri Nilai</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('.tampilModalBeriNilai').on('click', function() {
                var user_id = $(this).attr('data-user')
                var name = $(this).attr('data-name')
                var assignment_id = $(this).attr('data-assignment')
                var action = '/collection/' + user_id
                $('#collectionUserID').val(user_id)
                $('#collectionAssignmentID').val(assignment_id)
                $('#collectionName').val(name)
                $('#beriNilaiForm').attr('action', action)
            })
        })
    </script>
@endpush
