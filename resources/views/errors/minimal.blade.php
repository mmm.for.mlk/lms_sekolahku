@include('layouts.header')

<body class="inner_page error_404">
    <div class="full_container">
        <div class="container">
            <div class="center verticle_center full_height">
                <div class="error_page">
                    <br>
                    <h3>@yield('code')</h3>
                    <P>@yield('message')</P>
                    <div class="center"><a class="main_bt" href="/">Go To Home Page</a></div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</body>

</html>
