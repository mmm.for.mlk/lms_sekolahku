@extends('layouts.master')

@section('title')
    {{ $attendanceForm->section->course->name }}
@endsection


@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>{{ $attendanceForm->name }}</h2>
                    </div>
                    <div class="text-right">
                        <a href="{{ url('/attendance-recordpdf/' . $attendanceForm->id) }}"
                            class="btn btn-danger">-Pdf-</a>
                        <a href="{{ url('/attendance-recordexcel/' . $attendanceForm->id) }}"
                            class="btn btn-success">-Excel-</a>
                    </div>
                </div>
                <div class="table_section padding_infor_info">
                    <div class="table-responsive-sm">
                        <table id="attendanceRecords" class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Nama Murid</th>
                                    <th scope="col">Kehadiran</th>
                                    <th scope="col">Waktu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($participant as $item)
                                    @if ($item->user->role == 'student')
                                        <tr>
                                            <td> {{ $key = $key + 1 }} </td>
                                            <td> {{ $item->user->name }} </td>
                                            @if (($data = $item->user->attendanceRecord()->where('attendances_form_id', $attendanceForm->id)->first()) != null)
                                                <td> {{ $data->status }} </td>
                                                <td> {{ $data->date }} </td>
                                            @else
                                                <td> Belum presensi </td>
                                                <td> - </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
