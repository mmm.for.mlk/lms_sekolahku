@include('layouts/header')

<div class="container">
    <div class="center verticle_center full_height">
        <div class="login_section">
            <div class="logo_login">
                <div class="center">
                    <img width="210" src="images/logo/logo.png" alt="#" />
                </div>
            </div>
            <div class="login_form">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <fieldset>
                        <div class="field">
                            <label class="label_field">Email Address</label>
                            <input id="email" type="email" name="email" value="{{ old('email') }}"
                                class="@error('email') btn-outline-danger @enderror" placeholder="E-mail" required
                                autocomplete="email" autofocus />
                        </div>
                        <div class="field">
                            <label class="label_field">Password</label>
                            <input id="password" type="password" name="password"
                                class="@error('password') btn-outline-danger @enderror" placeholder="Password" required
                                autocomplete="current-password" />
                        </div>
                        @error('email')
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        @enderror
                        @error('password')
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        @enderror
                        <div class="field">
                            <label class="label_field hidden">hidden label</label>
                            <label class="form-check-label"><input class="form-check-input" type="checkbox"
                                    name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember
                                Me</label>
                        </div>
                        <div>
                            <label class="label_field hidden">hidden label</label>
                            <button type="submit" class="main_bt">{{ __('Login') }}</button>
                            <label class="label_field hidden">hidden label</label>
                            <button class="main_bt"><a class="text-white"
                                    href="{{ route('register') }}">{{ __('Register') }}</a></button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>


@include('layouts/footer')
