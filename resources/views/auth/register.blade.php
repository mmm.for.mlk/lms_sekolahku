@include('layouts/header')
<div class="container">
    <div class="center verticle_center full_height">
        <div class="login_section">
            <div class="logo_login">
                <div class="center">
                    <img width="210" src="images/logo/logo.png" alt="#" />
                </div>
            </div>
            <div class="login_form">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <fieldset>
                        <div class="field">
                            <label class="label_field">{{ __('Name') }}</label>
                            <input id="name" type="text" name="name" value="{{ old('name') }}"
                                class="@error('name') btn-outline-danger @enderror" placeholder="Nama" required
                                autocomplete="name" autofocus />
                        </div>
                        <div class="field">
                            <label class="label_field">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" name="email" value="{{ old('email') }}"
                                class="@error('email') btn-outline-danger @enderror" placeholder="E-mail" required
                                autocomplete="email" autofocus />
                        </div>
                        <div class="field">
                            <label class="label_field">{{ __('Password') }}</label>
                            <input id="password" type="password" name="password"
                                class="@error('password') btn-outline-danger @enderror" placeholder="Password" required
                                autocomplete="new-password" />
                        </div>
                        <div class="field">
                            <label class="label_field">RetypePassword</label>
                            <input id="password-confirm" type="password" name="password_confirmation"
                                class="@error('password') btn-outline-danger @enderror" placeholder="Retype Password"
                                required autocomplete="new-password" />
                        </div>
                        @error('name')
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        @enderror
                        @error('email')
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        @enderror
                        @error('password')
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        @enderror
                        <div class="field">
                            <label class="label_field hidden">hidden label</label>
                            <label class="form-check-label"><input class="form-check-input" type="checkbox"
                                    name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember
                                Me</label>
                            @if (Route::has('login'))
                                <a class="forgot" href="{{ route('login') }}">
                                    {{ __('Sudah punya akun? login') }}
                                </a>
                            @endif
                        </div>
                        <div class="field margin_0">
                            <label class="label_field hidden">hidden label</label>
                            <button type="submit" class="main_bt">{{ __('Register') }}</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

@include('layouts/footer')
