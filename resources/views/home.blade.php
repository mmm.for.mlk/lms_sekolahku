@extends('layouts.master')

@section('title')
    Home - Sekolahku
@endsection

@section('content')
    <div class="row">
        <!-- tab style 1 -->
        <div class="col-md-12">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>Selamat Datang di Portal LMS Sekolahku</h2>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="padding_infor_info">
                                <p>Portal LMS (Learning Management System) Sekolahku ini merupakan sebuah project yang
                                    dibangun dengan tujuan untuk membantu proses pembelajaran online sekolah-sekolah di Jawa
                                    Barat. Portal LMS ini menggunakan framework PHP yaitu Laravel versi 6.x.</p>
                                <p>Terdapat 3 role di LMS ini yaitu admin, teacher, dan student.</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="full white_shd margin_bottom_30">
                <div class="info_people">
                    <div class="user_info_cont">
                        <h4>Muhammad Ridwan Fauzi</h4>
                        <p>madwanz64@gmail.com</p>
                        <p class="p_status">Developer</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="full white_shd margin_bottom_30">
                <div class="info_people">
                    <div class="user_info_cont">
                        <h4>Mulki Zulkarnaen Nurfalah</h4>
                        <p>mmm.for.mlk@gmail.com</p>
                        <p class="p_status">Developer</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="full white_shd margin_bottom_30">
                <div class="info_people">
                    <div class="user_info_cont">
                        <h4>Yoga Setiawan</h4>
                        <p>setiawan.yoga995@gmail.com</p>
                        <p class="p_status">Developer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
