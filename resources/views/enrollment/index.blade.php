@extends('layouts.master')

@section('title')
    {{ $course->name }}
@endsection


@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>{{ $course->name }} Participant</h2>
                    </div>
                </div>
                <div class="table_section padding_infor_info">
                    <div class="table-responsive-sm">
                        <table id="attendanceRecords" class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Nama Siswa</th>
                                    <th scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($participant as $item)
                                    @if ($item->user->role == 'student')
                                        <tr>
                                            <td> {{ $key = $key + 1 }} </td>
                                            <td> {{ $item->user->name }} </td>
                                            <td class="text-center">
                                                <div class="row d-flex justify-content-center m-0">
                                                    <a href="/profile/{{ $item->user->id }}"
                                                        class="btn btn-info">Profile</a>
                                                    @if (Auth::user()->role != 'student')
                                                        <form
                                                            action="/course/{{ $course->id }}/enrollment/{{ $item->user->id }}"
                                                            method="post">
                                                            @csrf
                                                            @method('DELETE')

                                                            <button type="submit"
                                                                class="btn btn-outline-danger ml-3">Keluarkan</button>
                                                        </form>
                                                    @endif

                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @empty
                                    <h1>Belum ada Kehadiran</h1>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
