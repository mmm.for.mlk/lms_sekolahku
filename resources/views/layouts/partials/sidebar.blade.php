<nav id="sidebar">
    <div class="sidebar_blog_1">
        <div class="sidebar-header">
            <div class="logo_section">
                <a href="/"><img class="logo_icon img-responsive" src="{{ asset('images/logo/logo_icon.png') }}"
                        alt="#" /></a>
            </div>
        </div>
        <div class="sidebar_user_info">
            <div class="icon_setting"></div>
            <div class="user_profle_side">
                <div class="user_info">
                    <h6>{{ Auth::user()->name }}</h6>
                    <p><span class="online_animation"></span> Online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_blog_2">
        <h4>General</h4>
        <ul class="">
            <li class="active"><a href="/"><i class="fa fa-home fa-fw orange_color"></i>
                    <span>Home</span></a>
            </li>
            <li class="active"><a href="/dashboard"><i class="fa fa-dashboard fa-fw yellow_color"></i>
                    <span>Dashboard</span></a>
            </li>
            <li class="active"><a href="/course"><i class="fa fa-graduation-cap fa-fw blue2_color"></i>
                    <span>Courses</span></a>
            </li>
        </ul>
        @if (str_contains(url()->current(), 'course/') or str_contains(url()->current(), 'mod/'))
            <h4><a class="text-white" href="/course/{{ $course->id }}">{{ $course->name }}</a></h4>
            <ul class="">
                <li class="active"><a href="/course/{{ $course->id }}/enrollment"><i
                            class="fa fa-users blue2_color fa-fw" aria-hidden="true"></i>
                        <span>Participant</span></a>
                </li>
                {{-- <li class="active"><a href="/"><i class="fa fa-star" aria-hidden="true"></i>
                        <span>Grades</span></a>
                </li>
                <li class="active"><a href="/"><i class="fa fa-briefcase green_color fa-fw"
                            aria-hidden="true"></i>
                        <span>Assignment</span></a>
                </li> --}}
            </ul>
        @endif
        <ul class="d-xl-none">
            <li class="active"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }} <i class="fa fa-sign-out fa-fw red_color"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>
