@extends('layouts.master')

@section('title')
    Silahkan membuat Mata Pelajaran
@endsection

@section('content')
<form action="/course" method="post">
    @csrf
    <div class="form-group">
        <label>Nama Mata Pelajaran</label>
        <input type="text" class="form-control" name="name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Deskripsi Mata Pelajaran</label>
        <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Silahkan Buat Token</label>
        <span>Token otomatis dibuat apabila tidak di isi</span>
        <input type="text" maxlength="5" class="form-control" name="token">
    </div>
    @error('token')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection