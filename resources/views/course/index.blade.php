@extends('layouts.master')

@section('title')
    Courses
@endsection

@section('content')
    <div class="row">
        <!-- tab style 1 -->
        <div class="col-md-12">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head">
                    <div class="heading1 margin_0">
                        <h2>Course list</h2>
                    </div>
                    @if (Auth::user()->role != 'student')
                        <div class="text-right ">
                            <button type="button" class="btn btn-primary m-0" data-toggle="modal"
                                data-target="#modalCreateCourse">
                                Tambah Course
                            </button>
                        </div>
                    @endif
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="padding_infor_info">
                                <div class="row">

                                    @forelse ($course as $item)
                                        <div class="col-md-6 col-lg-4 mb-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{ $item->name }}</h5>
                                                    <p class="card-text">{{ Str::limit($item->description, 100) }}
                                                    </p>
                                                    <div class="d-flex justify-content-between">
                                                        <a href="{{ url('/course/' . $item->id) }}"
                                                            class="btn btn-primary">Akses course</a>

                                                        @if (Auth::user()->role != 'student')
                                                            <div class="dropdown_section m-0">
                                                                <div class="dropdown">
                                                                    <button type="button"
                                                                        class="btn btn-sm btn-outline-secondary"
                                                                        data-toggle="dropdown"><i
                                                                            class="fa fa-ellipsis-h text-dark"
                                                                            aria-hidden="true"></i></button>
                                                                    <div class="dropdown-menu">
                                                                        <form action="{{ url('/course/' . $item->id) }}"
                                                                            method="post">
                                                                            @csrf
                                                                            @method('delete')
                                                                            <button type="button"
                                                                                class="dropdown-item text-primary updateCourse"
                                                                                data-toggle="modal"
                                                                                data-target="#modalEditCourse"
                                                                                data-id="{{ $item->id }}"
                                                                                data-name="{{ $item->name }}"
                                                                                data-description="{{ $item->description }}">
                                                                                Edit Course
                                                                            </button>

                                                                            <button class="dropdown-item text-danger"
                                                                                type="submit">Delete</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        @if (Auth::user()->role != 'student')
                                            <p>Anda belum mengajar di course manapun, Silakan klik Tambah Course untuk
                                                membuat course baru.</p>
                                        @else
                                            <p>Anda belum mengikuti course manapun. Silakan klik menu courses dan pilih
                                                course
                                                yang ingin diikuti.</p>
                                        @endif
                                    @endforelse

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    <div class="modal fade" id="modalEditCourse" tabindex="-1" role="dialog" aria-labelledby="modalEditCourseTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Course</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateCourse" action="/course" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="updatename">Nama Mata Pelajaran</label>
                            <input type="text" class="form-control" id="updatename" name="updatename"
                                oninvalid="this.setCustomValidity('Nama mata pelajaran harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('updatename')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="updatedescription">Description</label>
                            <textarea class="form-control" id="updatedescription" name="updatedescription" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="updateenrollkey">Enrollkey baru</label>
                            <input type="text" class="form-control" id="updateenrollkey" name="updateenrollkey">
                            <small id="emailHelp" class="form-text text-muted">Jika tidak dicantumkan enrollkey baru, akan
                                digunakan enrollkey lama agar user bisa mengakses course ini.</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Update Course</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCreateCourse" tabindex="-1" role="dialog" aria-labelledby="modalCreateCourseTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Course</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/course" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nama Mata Pelajaran</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Nama mata pelajaran harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="token">Enrollkey</label>
                            <input type="text" class="form-control" id="token" name="token">
                            <small id="emailHelp" class="form-text text-muted">Enrollkey digunakan sebagai password saat
                                murid ingin mengikuti course ini. Jika tidak diisi, enrollkey akan dibuat secara
                                random</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah Course</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function() {
            $('.updateCourse').on('click', function() {
                var id = "/course/" + $(this).attr('data-id');
                var name = $(this).attr('data-name');
                var description = $(this).attr('data-description');
                $('#updateCourse').attr('action', id);
                $('#updatename').val(name);
                $('#updatedescription').val(description);
            });

        })
    </script>
@endpush
