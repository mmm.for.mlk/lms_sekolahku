@extends('layouts.master')

@section('title')
    {{ $course->name . ' - ' . $course->enrollkey }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            @if (Session::get('error_msg'))
                <div class="alert alert-danger">
                    {{ Session::get('error_msg') }}
                </div>
            @endif
            <div class="white_shd full margin_bottom_30">
                <div class="full inner_elements">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab_style1">
                                <div class="tabbar padding_infor_info">
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                                href="#nav-home" role="tab" aria-controls="nav-home"
                                                aria-selected="true">Description</a>
                                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                                href="#nav-profile" role="tab" aria-controls="nav-profile"
                                                aria-selected="false">Pengajar</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                            aria-labelledby="nav-home-tab">
                                            <p>{{ $course->description }}
                                            </p>
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                            aria-labelledby="nav-profile-tab">
                                            @foreach ($teacher as $item)
                                                @if ($item->user->role == 'teacher')
                                                    <p>{{ $item->user->name }} </p>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($enrolled == true)
        @forelse ($course->section as $item)
            <div class="row">
                <div class="col">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head d-flex justify-content-between">
                            <div class="heading1 margin_0">
                                <h2>{{ $item->name }}</h2>
                            </div>
                            @if (Auth::user()->role != 'student')
                                <form action="{{ url('/course/' . $course->id . '/section/' . $item->id) }}"
                                    method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            @endif
                        </div>
                        <div class="full progress_bar_inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="msg_list_main">
                                        <ul class="msg_list">
                                            {{-- red untuk resources --}}
                                            @foreach ($item->resources->sortBy('name') as $resource)
                                                <li style="border-left-color:red">
                                                    <span>
                                                        <span class="name_user"><a
                                                                href="{{ asset('resource/document/' . $resource->attachment) }}"
                                                                target="_blank"><i class="fa fa-file-pdf-o red_color fa-fw"
                                                                    aria-hidden="true"></i>
                                                                {{ $resource->name }}</a></span>
                                                        <span class="msg_user">{{ $resource->description }}</span>
                                                        @if (Auth::user()->role != 'student')
                                                            <span class="time_ago">
                                                                <button type="button"
                                                                    class="btn btn-sm btn-outline-info dropdown-toggle"
                                                                    data-toggle="dropdown">Action</button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item blue2_color"
                                                                        href="/mod/resource/{{ $resource->id }}/edit">Edit</a>
                                                                    <form action="/mod/resource/{{ $resource->id }}"
                                                                        method="post">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <input type="hidden" name="course_id"
                                                                            value="{{ $course->id }}">
                                                                        <button class="dropdown-item red_color"
                                                                            type="submit">Delete</button>
                                                                    </form>

                                                                </div>
                                                            </span>
                                                        @endif

                                                    </span>
                                                </li>
                                            @endforeach
                                            {{-- aqua untuk external_links --}}
                                            @foreach ($item->externalLinks as $externalLink)
                                                <li style="border-left-color:aqua">
                                                    <span>
                                                        <span class="name_user"><a href="{{ $externalLink->url }}"
                                                                target="_blank"><i
                                                                    class="fa fa-external-link blue2_color fa-fw"
                                                                    aria-hidden="true"></i>
                                                                {{ $externalLink->name }}</a></span>
                                                        <span
                                                            class="msg_user">{{ $externalLink->description }}</span>
                                                        @if (Auth::user()->role != 'student')
                                                            <span class="time_ago">
                                                                <button type="button"
                                                                    class="btn btn-sm btn-outline-info dropdown-toggle"
                                                                    data-toggle="dropdown">Action</button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item blue2_color"
                                                                        href="/mod/url/{{ $externalLink->id }}/edit">Edit</a>
                                                                    <form action="/mod/url/{{ $externalLink->id }}"
                                                                        method="post">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <input type="hidden" name="course_id"
                                                                            value="{{ $course->id }}">
                                                                        <button class="dropdown-item red_color"
                                                                            type="submit">Delete</button>
                                                                    </form>

                                                                </div>
                                                            </span>
                                                        @endif
                                                    </span>
                                                </li>
                                            @endforeach
                                            {{-- orange untuk attendance --}}
                                            @foreach ($item->attendanceForms as $attendanceForm)
                                                <li style="border-left-color:orange">
                                                    <span>
                                                        <span class="name_user"><a
                                                                href="/mod/attendance-form/{{ $attendanceForm->id }}"><i
                                                                    class="fa fa-calendar-check-o yellow_color fa-fw"
                                                                    aria-hidden="true"></i>
                                                                {{ $attendanceForm->name }}</a></span>
                                                        <span class="msg_user">Open Date :
                                                            {{ $attendanceForm->opendate }}</span>
                                                        <span class="msg_user">Due Date :
                                                            {{ $attendanceForm->duedate }}</span>
                                                        @if (Auth::user()->role != 'student')
                                                            <span class="time_ago">
                                                                <button type="button"
                                                                    class="btn btn-sm btn-outline-info dropdown-toggle"
                                                                    data-toggle="dropdown">Action</button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item blue2_color"
                                                                        href="/mod/attendance-form/{{ $attendanceForm->id }}/edit">Edit</a>
                                                                    <form
                                                                        action="/mod/attendance-form/{{ $attendanceForm->id }}"
                                                                        method="post">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <input type="hidden" name="course_id"
                                                                            value="{{ $course->id }}">
                                                                        <button class="dropdown-item red_color"
                                                                            type="submit">Delete</button>
                                                                    </form>

                                                                </div>
                                                            </span>
                                                        @endif
                                                    </span>
                                                </li>
                                            @endforeach
                                            {{-- seagreen untuk assignment --}}
                                            @foreach ($item->assignments as $assignment)
                                                <li style="border-left-color:seagreen">
                                                    <span>
                                                        <span class="name_user"><a
                                                                href="/mod/assignment/{{ $assignment->id }}"><i
                                                                    class="fa fa-briefcase green_color fa-fw"
                                                                    aria-hidden="true"></i>
                                                                {{ $assignment->name }}</a></span>
                                                        <span
                                                            class="msg_user">{{ $assignment->description }}</span>
                                                        @if (Auth::user()->role != 'student')
                                                            <span class="time_ago">
                                                                <button type="button"
                                                                    class="btn btn-sm btn-outline-info dropdown-toggle"
                                                                    data-toggle="dropdown">Action</button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item blue2_color"
                                                                        href="/mod/assignment/{{ $assignment->id }}/edit">Edit</a>
                                                                    <form action="/mod/assignment/{{ $assignment->id }}"
                                                                        method="post">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <input type="hidden" name="course_id"
                                                                            value="{{ $course->id }}">
                                                                        <button class="dropdown-item red_color"
                                                                            type="submit">Delete</button>
                                                                    </form>

                                                                </div>
                                                            </span>
                                                        @endif
                                                    </span>
                                                </li>
                                            @endforeach


                                            @if (Auth::user()->role != 'student')
                                                <li style="border: none" class="d-flex justify-content-center">
                                                    <div class="dropdown_section">
                                                        <div class="dropdown">
                                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                                data-toggle="dropdown"><i
                                                                    class="fa fa-plus-circle fa-fw fa-lg"
                                                                    aria-hidden="true"></i> Konten</button>
                                                            <div class="dropdown-menu">
                                                                <button id="resource" type="button"
                                                                    class="dropdown-item tampilModalBahanAjar"
                                                                    data-toggle="modal" data-target="#bahanAjarModal"
                                                                    data-id="{{ $item->id }}">Tambah Bahan
                                                                    Ajar</button>
                                                                <button type="button"
                                                                    class="dropdown-item tampilModalExternalLink"
                                                                    data-toggle="modal" data-target="#externalLinkModal"
                                                                    data-id="{{ $item->id }}">Tambah External
                                                                    Link</button>
                                                                <button type="button"
                                                                    class="dropdown-item tampilModalAttendance"
                                                                    data-toggle="modal" data-target="#attendanceModal"
                                                                    data-id="{{ $item->id }}">Tambah
                                                                    Attendance</button>
                                                                <button type="button"
                                                                    class="dropdown-item tampilModalAssignment"
                                                                    data-toggle="modal" data-target="#assignmentModal"
                                                                    data-id="{{ $item->id }}">Tambah
                                                                    Assignment</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty

            @if (Auth::user()->role != 'student')
                <div class="row">
                    <div class="col">
                        <div class="white_shd full margin_bottom_30 ">
                            <div class="full inner_elements">
                                <div class="full graph_head">
                                    <div class="heading1 margin_0">
                                        <h2>Belum ada materi yang ditambahkan, silakan klik (+) section untuk memulai</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col">
                        <div class="white_shd full margin_bottom_30 ">
                            <div class="full inner_elements">
                                <div class="full graph_head">
                                    <div class="heading1 margin_0">
                                        <h2>Belum ada materi yang ditambahkan, tunggu sampai guru menambahkan materi</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforelse
        @if (Auth::user()->role != 'student')
            <div class="text-center mb-3">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSection">
                    <i class="fa fa-plus-circle fa-fw fa-lg" aria-hidden="true"></i> Section
                </button>
            </div>
        @endif
    @else
        <div class="row">
            <div class="col">
                <div class="white_shd full margin_bottom_30">
                    <div class="full graph_head">
                        <div class="heading1 margin_0">
                            <h2>Enrollment</h2>
                        </div>
                    </div>
                    <div class="full progress_bar_inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="padding_infor_info">
                                    <form action="/course/{{ $course->id }}/enrollment" method="post">
                                        @csrf
                                        <div class="input-group mb-3">
                                            <input type="hidden" name="course_id" value="{{ $course->id }}">
                                            <input type="text" name="enrollkey" class="form-control"
                                                placeholder="Masukkan enrollment key" required>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-outline-secondary"
                                                    type="button">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection


@section('modal')
    <div class="modal fade" id="bahanAjarModal" tabindex="-1" role="dialog" aria-labelledby="bahanAjarModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Bahan Ajar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/mod/resource" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="section_id" id="resourceSectionID">
                        <div class="form-group">
                            <label for="name">Judul Bahan Ajar</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Nama mata pelajaran harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="attachment">Attachment</label>
                            <input type="file" name="attachment" class="form-control-file" id="attachment" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah Bahan Ajar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="externalLinkModal" tabindex="-1" role="dialog" aria-labelledby="externalLinkModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah External Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/mod/url" method="post">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="section_id" id="urlSectionID">
                        <div class="form-group">
                            <label for="name">Judul External Link</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Nama External Link harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="url">Url</label>
                            <input type="text" class="form-control" id="url" name="url"
                                oninvalid="this.setCustomValidity('Url harus diisi')" required>
                            @error('url')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah External Link</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="attendanceModal" tabindex="-1" role="dialog" aria-labelledby="attendanceModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Attendance Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/mod/attendance-form" method="post">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="section_id" id="attendanceSectionID">
                        <div class="form-group">
                            <label for="name">Judul Presensi</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Judul absen harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opendate">Open date:</label>
                                    <input type="date" name="opendate" class="form-control" id="opendate" required>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="openhour">Jam</label>
                                    <input type="number" min="0" max="23" class="form-control" id="openhour"
                                        name="openhour" oninvalid="this.setCustomValidity('Judul tugas harus diisi')"
                                        required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="openminute">Menit</label>
                                    <input type="number" min="0" max="59" class="form-control" id="openminute"
                                        name="openminute" oninvalid="this.setCustomValidity('Judul tugas harus diisi')"
                                        required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="duedate">Due date:</label>
                                    <input type="date" name="duedate" class="form-control" id="duedate" required>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="duehour">Jam</label>
                                    <input type="number" min="0" max="23" class="form-control" id="duehour"
                                        name="duehour" required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="dueminute">Menit</label>
                                    <input type="number" min="0" max="59" class="form-control" id="dueminute"
                                        name="dueminute" required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah Presensi</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="assignmentModal" tabindex="-1" role="dialog" aria-labelledby="assignmentModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Assignment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/mod/assignment" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="section_id" id="assignmentSectionID">
                        <div class="form-group">
                            <label for="name">Judul Tugas</label>
                            <input type="text" class="form-control" id="name" name="name"
                                oninvalid="this.setCustomValidity('Judul tugas harus diisi')" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opendate">Open date:</label>
                                    <input type="date" name="opendate" class="form-control" id="opendate" required>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="openhour">Jam</label>
                                    <input type="number" min="0" max="23" class="form-control" id="openhour"
                                        name="openhour" required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="openminute">Menit</label>
                                    <input type="number" min="0" max="59" class="form-control" id="openminute"
                                        name="openminute" required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="duedate">Due date:</label>
                                    <input type="date" name="duedate" class="form-control" id="duedate" required>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="duehour">Jam</label>
                                    <input type="number" min="0" max="23" class="form-control" id="duehour"
                                        name="duehour" required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="dueminute">Menit</label>
                                    <input type="number" min="0" max="59" class="form-control" id="dueminute"
                                        name="dueminute" required>
                                    <small id="emailHelp" class="form-text text-muted"></small>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="attachment">Attachment</label>
                            <input type="file" name="attachment" class="form-control-file" id="attachment">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah Tugas</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalSection" tabindex="-1" role="dialog" aria-labelledby="modalSectionTitle"
        aria-hidden="true">
        <form action="{{ url('/course/' . $course->id . '/section') }}" method="post">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Section</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            @csrf
                            <label for="name">Nama Materi</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary m-0">Tambah Course</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('.tampilModalBahanAjar').on('click', function() {
                var section_id = $(this).attr('data-id')
                $('#resourceSectionID').val(section_id)
            })

            $('.tampilModalExternalLink').on('click', function() {
                var section_id = $(this).attr('data-id')
                $('#urlSectionID').val(section_id)
            })

            $('.tampilModalAssignment').on('click', function() {
                var section_id = $(this).attr('data-id')
                $('#assignmentSectionID').val(section_id)
            })

            $('.tampilModalAttendance').on('click', function() {
                var section_id = $(this).attr('data-id')
                $('#attendanceSectionID').val(section_id)
            })
        })
    </script>
@endpush
