@extends('layouts.master')

@section('title')
    {{ $attendanceForm->section->course->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>Update {{ $attendanceForm->name }}</h2>
                    </div>
                </div>
                <div class="full progress_bar_inner">
                    <div class="padding_infor_info">
                        <form action="/mod/attendance-form/{{ $attendanceForm->id }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Judul Presensi</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    oninvalid="this.setCustomValidity('Judul absen harus diisi')"
                                    value="{{ $attendanceForm->name }}" required>
                                <small id="emailHelp" class="form-text text-muted"></small>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="opendate">Open date:</label>
                                        <input type="date" name="opendate" class="form-control" id="opendate"
                                            value="{{ $opendate->format('Y-m-d') }}" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="openhour">Jam</label>
                                                <input type="number" min="0" max="23" class="form-control" id="openhour"
                                                    name="openhour"
                                                    oninvalid="this.setCustomValidity('Judul tugas harus diisi')"
                                                    value="{{ $opendate->format('H') }}" required>
                                                <small id="emailHelp" class="form-text text-muted"></small>
                                                @error('name')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="openminute">Menit</label>
                                                <input type="number" min="0" max="59" class="form-control" id="openminute"
                                                    name="openminute"
                                                    oninvalid="this.setCustomValidity('Judul tugas harus diisi')"
                                                    value="{{ $opendate->format('i') }}" required>
                                                <small id="emailHelp" class="form-text text-muted"></small>
                                                @error('name')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="duedate">Due date:</label>
                                        <input type="date" name="duedate" class="form-control" id="duedate"
                                            value="{{ $duedate->format('Y-m-d') }}" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="duehour">Jam</label>
                                                <input type="number" min="0" max="23" class="form-control" id="duehour"
                                                    name="duehour" value="{{ $duedate->format('H') }}" required>
                                                <small id="emailHelp" class="form-text text-muted"></small>
                                                @error('name')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="dueminute">Menit</label>
                                                <input type="number" min="0" max="59" class="form-control" id="dueminute"
                                                    name="dueminute" value="{{ $duedate->format('i') }}" required>
                                                <small id="emailHelp" class="form-text text-muted"></small>
                                                @error('name')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Tambah Presensi</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
