@extends('layouts.master')

@section('title')
    {{ $course->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="white_shd full margin_bottom_30">
                <div class="full graph_head d-flex justify-content-between">
                    <div class="heading1 margin_0">
                        <h2>{{ $attendanceForm->name }}</h2>
                    </div>
                </div>
                @if (date_create('now', timezone_open('Asia/Jakarta')) > $opendate)
                    <div class="full progress_bar_inner">
                        <div class="padding_infor_info">
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Attendance Record</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    @if ($attendanceRecord != null)
                                        <p>{{ $attendanceRecord->status }}</p>
                                    @else
                                        <p>Belum Presensi</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Open Date</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <p>{{ $opendate->format('\J\a\m H:i:s \t\a\n\g\g\a\l d F Y') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">Due Date</p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    <p>{{ $duedate->format('\J\a\m H:i:s \t\a\n\g\g\a\l d F Y') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-3">
                                    <p class="font-weight-bold">
                                        @if ($attendanceRecord != null)
                                            Jam Presensi
                                        @else
                                            Waktu Tersisa
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-8 col-lg-9">
                                    @if ($attendanceRecord != null)
                                        {{ date_create_from_format('Y-m-d H:i:s', $attendanceRecord->date, timezone_open('Asia/Jakarta'))->format('\J\a\m H:i:s \t\a\n\g\g\a\l d F Y') }}
                                    @else
                                        @if (date_create('now') > $duedate)
                                            <p>Anda terlambat
                                                {{ $interval->days . ' hari ' . $interval->h . ' jam ' . $interval->i . ' menit ' . $interval->s . ' detik' }}
                                            </p>
                                        @else
                                            <p>{{ $interval->days . ' hari ' . $interval->h . ' jam ' . $interval->i . ' menit ' . $interval->s . ' detik' }}
                                            </p>
                                        @endif
                                    @endif

                                </div>
                            </div>
                            @if ($attendanceRecord == null)
                                <div class="row">
                                    <div class="col d-flex justify-content-center">
                                        <form action="/attendance-record" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                            <input type="hidden" name="attendance_form_id"
                                                value="{{ $attendanceForm->id }}">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary w-100">Presensi</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @else
                    <div class="full progress_bar_inner">
                        <div class="padding_infor_info">
                            <p>Presensi ini belum dapat diakses. Anda dapat mengaksesnya setelah
                                {{ $opendate->format('\J\a\m H:i \t\a\n\g\g\a\l d F Y') }}</p>
                            <a href="/course/{{ $course->id }}" class="btn btn-info">Kembali</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
