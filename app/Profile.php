<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ['id','picture','bio','social_media'];
    public function user(){
        return $this->belongsTo('App\UserModel');
    }

}
