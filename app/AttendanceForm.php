<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceForm extends Model
{
    protected $table = 'attendances_form';
    protected $fillable = ['name','opendate','duedate','section_id'];
    
    public function section()
    {
        return $this->belongsTo('App\Section');
    }
    public function attendanceRecords()
    {
        return $this->hasMany('App\AttendanceRecord', 'attendances_form_id');
    }
}
