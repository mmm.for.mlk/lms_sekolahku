<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';
    protected $fillable = ['name', 'description','attachment','section_id'];
    public function section()
    {
        return $this->belongsTo('App\Section');
    }
}
