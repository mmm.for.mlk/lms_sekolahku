<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'section';
    protected $fillable = ["name","course_id"];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
    public function assignments()
    {
        return $this->hasMany('App\Assignment');
    }
    public function attendanceForms()
    {
        return $this->hasMany('App\AttendanceForm');
    }
    public function externalLinks()
    {
        return $this->hasMany('App\ExternalLink');
    }
    public function resources()
    {
        return $this->hasMany('App\Resource');
    }
}
