<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = "users";
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function enrollments()
    {
        return $this->hasMany('App\Enrollment', 'user_id');
    }
    public function collections()
    {
        return $this->hasMany('App\Collection', 'user_id');
    }
    public function attendanceRecord()
    {
        return $this->hasMany('App\AttendanceRecord', 'user_id');
    }
    public function profile(){
        return $this->hasOne('App\Profile', 'id');
    }
}
