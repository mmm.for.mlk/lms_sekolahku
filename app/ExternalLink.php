<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExternalLink extends Model
{
    protected $table = 'external_links';
    protected $fillable = ['name','description','url','section_id'];
    
    public function section()
    {
        return $this->belongsTo('App\Section');
    }
}
