<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = 'collections';
    protected $fillable = ['user_id', 'assignment_id', 'attachment', 'score'];

    public function user()
    {
        return $this->belongsTo('App\UserModel');
    }

    public function assignment()
    {
        return $this->belongsTo('App\Assignment');
    }
}
