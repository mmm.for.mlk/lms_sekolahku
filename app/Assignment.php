<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $table = 'assignments';
    protected $fillable = ['name', 'description', 'opendate', 'duedate', 'attachment', 'section_id'];

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function collections()
    {
        return $this->hasMany('App\Collection');
    }
}
