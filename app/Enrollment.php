<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    protected $table = 'enrollments';
    protected $fillable = ['user_id', 'course_id'];

    public function user()
    {
        return $this->belongsTo('App\UserModel');
    }
    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
