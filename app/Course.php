<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ["name", "description", "enrollkey"];

    public function enrollments()
    {
        return $this->hasMany('App\Enrollment');
    }

    public function section()
    {
        return $this->hasMany('App\Section');
    }

    public function sectionResources(){
        return $this->hasManyThrough('App\Resource', 'App\Section');
    }


}
