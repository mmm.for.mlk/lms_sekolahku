<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use App\AttendanceForm;
use App\AttendanceRecord;
use App\Enrollment;
use DB;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Exports\AttendanceRecordExport;
use Excel;

class AttendanceRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            'attendance_form_id' => 'required',
        ]);
        $data = AttendanceRecord::where('attendances_form_id', $request->attendance_form_id)->where('user_id', Auth::id())->first();
        // dd(Auth::id());
        if($data != null){
            return redirect()->back()->with('error_msg', 'Anda sudah melakukan Presensi');
        } else {
            $attendanceForm = AttendanceForm::find($request->attendance_form_id);

            $opendate = date_create_from_format('Y-m-d H:i:s', $attendanceForm->opendate,timezone_open('Asia/Jakarta'));
            $batastelat = date_add($opendate, date_interval_create_from_date_string('600 seconds'));
            $waktusekarang = date_create('now', timezone_open('Asia/Jakarta'));
            
            if ($waktusekarang > $batastelat){
            $status = 'Terlambat';                
            } else {
                $status = 'Tepat Waktu';
            }
            $attendance = new AttendanceRecord;
            $attendance->attendances_form_id = $request->attendance_form_id; 
            $attendance->user_id = Auth::id();
            $attendance->date = $waktusekarang;
            $attendance->status = $status;
            // dd($attendance);
            $attendance->save();
            Alert::success('Berhasil', 'Absen berhasil dilakukan');
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attendanceForm = AttendanceForm::find($id);
        $participant = Enrollment::with('user')->where('course_id', $attendanceForm->section->course->id)->get()->sortBy('user.name');
        $key = 0;
        return view('attendance-record.show', compact('attendanceForm', 'participant', 'key'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function printpdf($attendance_form_id)
    {
        $data['data'] = AttendanceForm::find($attendance_form_id)->load('attendanceRecords.user')->toArray();
        //dd($data);
        $pdf = PDF::loadView('section.print-attendance', $data);
        return $pdf->stream('invoice.pdf');
    }

    public function printexcel($attendance_form_id)
    {
        $data['data'] = AttendanceForm::find($attendance_form_id)->load('attendanceRecords.user')->toArray();
        $arrData = [
            ['No', 'Nama Murid', 'Status', 'Waktu Presensi']
          ];
          foreach ($data['data']['attendance_records'] as $key => $item) {
            array_push($arrData, [
              $key + 1,
              $item['user']['name'],
              $item['status'],
              $item['date'],
            ]);
          }
              
        $export = new AttendanceRecordExport($arrData);
    
        return Excel::download($export, 'absensi-'. $data['data']['duedate'] . " - " . $data['data']['name'] . '.xlsx');
    }
}
