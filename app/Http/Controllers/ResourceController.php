<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use File;
use App\Course;
use App\Resource;
use Illuminate\Http\Request;

class ResourceController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'attachment' => 'required|mimes:pdf',
            'section_id' => 'required',
        ]);

        $attachment = $request->attachment;
        $newAttachment = time() . '-' . $attachment->getClientOriginalName();

        $resource = new Resource;
        $resource->name = $request->name;
        $resource->description = $request->description;
        $resource->attachment = $newAttachment;
        $resource->section_id = $request->section_id;
        $resource->save();
        
        $attachment->move('resource/document/', $newAttachment);
        Alert::success('Berhasil', 'Bahan Ajar berhasil ditambahkan');
        return redirect('course/'.$resource->section->course->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
    public function edit($id)
    {
        $resource = Resource::findOrFail($id);
        if(Auth::user()->role != 'student') {
            $course = Course::find($resource->section->course->id);
            return view('resource.edit', compact('resource', 'course'));
        } else {
        return redirect('/course/'.$resource->section->course->id);
        }
    }
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $resource = Resource::find($id);
        $resource->name = $request->name;
        $resource->description = $request->description;
        if($request->attachment != NULL) {
            $attachment = $request->attachment;
            $newAttachment = time() . '-' . $attachment->getClientOriginalName();
            $path = "resource/document/";
            File::delete($path.$resource->attachment);
            $resource->attachment = $newAttachment;
        }
        $resource->save();
        
        if($request->attachment != NULL) {
        $attachment->move('resource/document/', $newAttachment);
        }
        Alert::success('Berhasil', 'Bahan Ajar berhasil diedit');
        return redirect('course/'.$resource->section->course->id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $resource = Resource::findOrFail($id);
        $path = "resource/document/";
        File::delete($path.$resource->attachment);
        Resource::destroy($id);
        Alert::success('Berhasil', 'Bahan Ajar berhasil dihapus');
        return redirect('/course/'.$request->course_id);
    }
}
