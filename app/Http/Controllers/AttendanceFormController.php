<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use App\AttendanceForm;
use App\AttendanceRecord;
use App\Course;
use App\Enrollment;
use App\Section;
use Illuminate\Http\Request;

class AttendanceFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'section_id' => 'required',
            'opendate' => 'required|date',
            'openhour' => 'required|min:0|max:23',
            'openminute' => 'required|min:0|max:59',
            'duedate' => 'required|date',
            'duehour' => 'required|min:0|max:23',
            'dueminute' => 'required|min:0|max:59',
        ]);

        $opendate = date_create($request->opendate, timezone_open('Asia/Jakarta'));
        date_time_set($opendate, $request->openhour, $request->openminute);
        $duedate = date_create($request->duedate, timezone_open('Asia/Jakarta'));
        date_time_set($duedate, $request->duehour, $request->dueminute);

        // dd($request->all());

        $section = Section::find($request->section_id);
        if($opendate > $duedate) {
            return redirect('/course/'.$section->course->id);
        } else {
            $attendance = new AttendanceForm;
            $attendance->name = $request->name;
            $attendance->opendate = $opendate->format('Y-m-d H:i:s');
            $attendance->duedate = $duedate->format('Y-m-d H:i:s');
            $attendance->section_id = $request->section_id;
            $attendance->save();
            Alert::success('Berhasil', 'Form presensi berhasil dibuat');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attendanceForm = AttendanceForm::findOrFail($id);
        $enrolled = Enrollment::where([['user_id', Auth::id()], ['course_id', $attendanceForm->section->course->id]])->count() == 1;
        if($enrolled == true) {
            if(Auth::user()->role != 'student') {
                return redirect('/attendance-record/'.$id);
            }else {
                $course = Course::find($attendanceForm->section->course->id);
                $opendate = date_create_from_format('Y-m-d H:i:s', $attendanceForm->opendate,timezone_open('Asia/Jakarta'));
                $duedate = date_create_from_format('Y-m-d H:i:s', $attendanceForm->duedate, timezone_open('Asia/Jakarta'));
                $interval = date_create('now', timezone_open('Asia/Jakarta'))->diff($duedate);
                $attendanceRecord = AttendanceRecord::where([['user_id', Auth::id()], ['attendances_form_id', $id]])->first();
                return view('attendance.show', compact('course','attendanceForm', 'opendate','duedate','interval','attendanceRecord'));
            }
        } else {
            return redirect('/course/'.$attendanceForm->section->course->id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role != 'student') {
            $attendanceForm = AttendanceForm::findOrFail($id);
            $course = Course::find($attendanceForm->section->course->id);
            $opendate = date_create_from_format('Y-m-d H:i:s', $attendanceForm->opendate,timezone_open('Asia/Jakarta'));
            $duedate = date_create_from_format('Y-m-d H:i:s', $attendanceForm->duedate, timezone_open('Asia/Jakarta'));
            $interval = date_create('now', timezone_open('Asia/Jakarta'))->diff($duedate);
            $attendanceRecord = AttendanceRecord::where([['user_id', Auth::id()], ['attendances_form_id', $id]])->first();
            return view('attendance.edit', compact('course','attendanceForm', 'opendate','duedate','interval','attendanceRecord'));
        } else {
            return redirect('/mod/attendance-form/'.$id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'opendate' => 'required|date',
            'openhour' => 'required|min:0|max:23',
            'openminute' => 'required|min:0|max:59',
            'duedate' => 'required|date',
            'duehour' => 'required|min:0|max:23',
            'dueminute' => 'required|min:0|max:59',
        ]);

        $opendate = date_create($request->opendate, timezone_open('Asia/Jakarta'));
        date_time_set($opendate, $request->openhour, $request->openminute);
        $duedate = date_create($request->duedate, timezone_open('Asia/Jakarta'));
        date_time_set($duedate, $request->duehour, $request->dueminute);
        
        $attendance = AttendanceForm::find($id);
        if($opendate > $duedate) {
            Alert::error('Error', 'Due date tidak dapat lebih awal');
            return redirect()->back();
        } else {
            $attendance->name = $request->name;
            $attendance->opendate = $opendate->format('Y-m-d H:i:s');
            $attendance->duedate = $duedate->format('Y-m-d H:i:s');
            $attendance->save();
            Alert::success('Berhasil', 'Form presensi berhasil diedit');
            return redirect('/course/'.$attendance->section->course->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AttendanceForm::destroy($id);
        Alert::success('Berhasil', 'Form presensi berhasil dihapus');
        return redirect()->back();
    }
}
