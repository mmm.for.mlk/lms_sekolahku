<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use App\Course;
use App\ExternalLink;
use Illuminate\Http\Request;

class ExternalLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'url' => 'required',
            'section_id' => 'required',
        ]);

        $externalLink = new ExternalLink;
        $externalLink->name = $request->name;
        $externalLink->description = $request->description;
        $externalLink->url = $request->url;
        $externalLink->section_id = $request->section_id;
        $externalLink->save();
        Alert::success('Berhasil', 'External Link berhasil ditambahkan');
        return redirect('course/'.$externalLink->section->course->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url = ExternalLink::findOrFail($id);
        if(Auth::user()->role != 'student') {
            $course = Course::find($url->section->course->id);
            return view('url.edit', compact('url', 'course'));
        } else {
        return redirect('/course/'.$url->section->course->id);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'url' => 'required'
        ]);

        $url = ExternalLink::find($id);
        $url->name = $request->name;
        $url->description = $request->description;
        $url->url = $request->url;
        $url->save();

        Alert::success('Berhasil', 'External Link berhasil diedit');
        return redirect('course/'.$url->section->course->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        ExternalLink::destroy($id);
        Alert::success('Berhasil', 'External Link berhasil dihapus');
        return redirect('/course/'.$request->course_id);
    }
}
