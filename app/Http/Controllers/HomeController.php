<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use Hash;
use App\Course;
use App\Enrollment;
use App\UserModel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        if (Auth::user()->role == 'admin') {
            $teacher = UserModel::where('role', 'teacher')->get()->count();
            $student = UserModel::where('role', 'student')->get()->count();
            $course = Course::all()->count();
            return view('dashboard.admin', compact('teacher', 'student','course'));
        } else {
            $enrollments = UserModel::find(Auth::id())->enrollments;
            return view('dashboard.index', compact('enrollments'));
        }
    }
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'role' => 'required'
        ]);
        if (Auth::user()->role == 'admin') {
            $user = new UserModel;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            if ($request->role == 'teacher') {
                $user->role = 'teacher';
            } else {
                $user->role = 'student';
            }
            $user->save();

            Alert::success('Berhasil', 'Berhasil menambahkan akun '.$user->role.' baru');

            return redirect('/dashboard');
        } else {
            return redirect('/dashboard');
        }
    }

}
