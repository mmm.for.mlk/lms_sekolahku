<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use App\Course;
use App\Enrollment;
use App\User;
use Illuminate\Http\Request;

class EnrollmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        if (Enrollment::where([['user_id', Auth::id()], ['course_id', $course_id]])->count() == 1) {
            $course = Course::find($course_id);
            $participant = Enrollment::with('user')->where('course_id', $course_id)->get()->sortBy('user.name');
            $key = 0;
            return view('enrollment.index', compact('participant', 'course', 'key'));
        } else {
            return redirect('/course/'.$course_id);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Course::where([['id', $request->course_id], ['enrollkey', $request->enrollkey]])->count() == 1) {
            $enrollment = new Enrollment;
            $enrollment->user_id = Auth::id();
            $enrollment->course_id = $request->course_id;
            $enrollment->save();
            Alert::success('Berhasil', 'Enrollment berhasil dilakukan');
            return redirect('/course/' . $request->course_id);
        } else {

            return redirect('/enrollment/' . $request->course_id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function show($enrollment)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function edit(Enrollment $enrollment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enrollment $enrollment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function destroy($course_id, $enrollment)
    {
        $temp = Enrollment::where('course_id', $course_id)->where( 'user_id', $enrollment)->delete();
        Alert::success('Berhasil', 'User berhasil dikeluarkan');
        return redirect()->back();
    }
}
