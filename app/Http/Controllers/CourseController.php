<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use App\Course;
use App\Enrollment;
use App\Section;
use App\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;



class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::all();
        return view('course.index', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'description' => 'required',
            ],
            [
                'name.required' => 'Mata Pelajaran harus diisi',
                'description.required' => 'Deskripsi harus diisi',
            ]
        );

        if ($request->token == null) {
            $request->token = Str::random(5);
        };
        $course = new Course;
        $course->name = $request->name;
        $course->description = $request->description;
        $course->enrollkey = $request->token;
        $course->save();

        $enrollment = new Enrollment;
        $enrollment->user_id = Auth::id();
        $enrollment->course_id = $course->id;
        $enrollment->save();

        Alert::success('Berhasil', 'Course '.$course->name.' berhasil dibuat');

        return redirect('/course/' . $course->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enrolled = Enrollment::where([['user_id', Auth::id()], ['course_id', $id]])->count() == 1;
        $course = Course::findOrFail($id);
        $teacher = Enrollment::where('course_id', $id)->get();
        return view('course.show', compact('course', 'teacher', 'enrolled'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'updatename' => 'required',
            ],
            [
                'updatename.required' => 'Mata Pelajaran harus diisi',
            ]
        );
        $course = Course::find($id);
        $course->name = $request->updatename;
        $course->description = $request->updatedescription;
        if($request->updateenrollkey != NULL){
            $course->enrollkey = $request->updateenrollkey;
        }
        $course->save();
        Alert::success('Berhasil', 'Course '.$course->name.' berhasil diedit');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();
        Alert::success('Berhasil', 'Course '.$course->name.' berhasil dihapus');
        return redirect()->back();
    }
}
