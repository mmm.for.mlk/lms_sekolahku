<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use File;
use App\Profile;
use App\UserModel;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (UserModel::find(Auth::id())->profile == NULL){
            return redirect('/profile/create');
        } else {
            return redirect('/profile/'.Auth::id());
        }
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'user_id' => 'required',
            'picture' => 'required|mimes:jpeg,jpg,png|max:2200',
            'bio' => 'required',
            'social_media' => 'required',          
            
        ]);

        $gambar = $request->picture;
        $newGambar = time() . '-' . $gambar->getClientOriginalName();
      
        $profile = Profile::updateOrCreate(
            ['id' => $request->user_id],
            ['id'=>$request->user_id,'picture' => $newGambar, 'bio' => $request->bio, 'social_media' => $request->social_media]
        );
        
        $gambar->move('picture/', $newGambar);
     
        return redirect('/profile/'.$request->user_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = UserModel::findOrFail($id);
        return view ('profile.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = profile::findOrFail($id);
        $user = user::all();
        
        return view ('profile.edit', compact('profile','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'picture' => 'required|mimes:jpeg,jpg,png|max:2200',
            'bio' => 'required',
            'social_media' => 'required',
            'user_id' => 'required',
            
        ]);
        $profile = profile::find($id);
        if($request->has('picture')){
            $path = "picture/";
            File::delete($path . $profile->picture);
            $gambar = $request->picture;
            $newGambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('picture/', $newGambar);
            $profile->picture = $newGambar;
        }
        $profile = Profile::updateOrCreate(
            ['id' => $request->user_id],
            ['id'=>$request->user_id,'picture' => $newGambar, 'bio' => $request->bio, 'social_media' => $request->social_media]
        );

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = profile::find($id);
        File::delete($path . $profile->picture);
        $profile->delete();

        return redirect('/profile');
    }
}
