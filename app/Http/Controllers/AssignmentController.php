<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use File;
use App\Assignment;
use App\Collection;
use App\Course;
use App\Enrollment;
use App\Section;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'attachment' => 'mimes:pdf',
            'section_id' => 'required',
            'opendate' => 'required|date',
            'openhour' => 'required|min:0|max:23',
            'openminute' => 'required|min:0|max:59',
            'duedate' => 'required|date',
            'duehour' => 'required|min:0|max:23',
            'dueminute' => 'required|min:0|max:59',
        ]);
        
        $opendate = date_create($request->opendate, timezone_open('Asia/Jakarta'));
        date_time_set($opendate, $request->openhour, $request->openminute);
        $duedate = date_create($request->duedate, timezone_open('Asia/Jakarta'));
        date_time_set($duedate, $request->duehour, $request->dueminute);

        $section = Section::find($request->section_id);
        if($opendate > $duedate) {
            return redirect('/course/'.$section->course->id);
        } else {
            $assignment = new Assignment;
            $assignment->name = $request->name;
            $assignment->description = $request->description;
            $assignment->opendate = $opendate->format('Y-m-d H:i:s');
            $assignment->duedate = $duedate->format('Y-m-d H:i:s');
            if($request->attachment != NULL){
                $attachment = $request->attachment;
                $newAttachment = time() . '-' . $attachment->getClientOriginalName();
                $assignment->attachment = $newAttachment;
            }
            $assignment->section_id = $request->section_id;
            $assignment->save();

            if($request->attachment != NULL){
            $attachment->move('resource/attachment/', $newAttachment);
        }
        Alert::success('Berhasil', 'Tugas berhasil dibuat');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assignment = Assignment::findOrFail($id);
        $enrolled = Enrollment::where([['user_id', Auth::id()], ['course_id', $assignment->section->course->id]])->count() == 1;
        if($enrolled == true) {
            if(Auth::user()->role != 'student') {
                return redirect('/collection/'.$id);
            }else {
            $course = Course::find($assignment->section->course->id);
            $opendate = date_create_from_format('Y-m-d H:i:s', $assignment->opendate,timezone_open('Asia/Jakarta'));
            $duedate = date_create_from_format('Y-m-d H:i:s', $assignment->duedate, timezone_open('Asia/Jakarta'));
            $interval = date_create('now', timezone_open('Asia/Jakarta'))->diff($duedate);
            $assignmentstatus = Collection::where([['user_id', Auth::id()], ['assignment_id', $id]])->first();
            return view('assignment.show', compact('course','assignment', 'opendate', 'duedate', 'interval', 'assignmentstatus'));
            }
        } else {
            return redirect('/course/'.$assignment->section->course->id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role != 'student') {
            $assignment = Assignment::findOrFail($id);
            $course = Course::find($assignment->section->course->id);
            $opendate = date_create_from_format('Y-m-d H:i:s', $assignment->opendate,timezone_open('Asia/Jakarta'));
            $duedate = date_create_from_format('Y-m-d H:i:s', $assignment->duedate, timezone_open('Asia/Jakarta'));
            $interval = date_create('now', timezone_open('Asia/Jakarta'))->diff($duedate);
            $assignmentstatus = Collection::where([['user_id', Auth::id()], ['assignment_id', $id]])->first();
            return view('assignment.edit', compact('course','assignment', 'opendate', 'duedate', 'interval', 'assignmentstatus'));
    } else {
        return redirect('/mod/assignment/'.$id);
    }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'attachment' => 'mimes:pdf',
            'opendate' => 'required|date',
            'openhour' => 'required|min:0|max:23',
            'openminute' => 'required|min:0|max:59',
            'duedate' => 'required|date',
            'duehour' => 'required|min:0|max:23',
            'dueminute' => 'required|min:0|max:59',
        ]);
        
        $opendate = date_create($request->opendate, timezone_open('Asia/Jakarta'));
        date_time_set($opendate, $request->openhour, $request->openminute);
        $duedate = date_create($request->duedate, timezone_open('Asia/Jakarta'));
        date_time_set($duedate, $request->duehour, $request->dueminute);

        $assignment = Assignment::find($id);
        if($opendate > $duedate) {
            Alert::error('Error', 'Due date tidak dapat lebih awal');
            return redirect('/course/'.$assignment->section->course->id);
        } else {
            $assignment->name = $request->name;
            $assignment->description = $request->description;
            $assignment->opendate = $opendate->format('Y-m-d H:i:s');
            $assignment->duedate = $duedate->format('Y-m-d H:i:s');
            if($request->attachment != NULL){
                $attachment = $request->attachment;
                $newAttachment = time() . '-' . $attachment->getClientOriginalName();
                $path = "resource/attachment/";
                File::delete($path.$assignment->attachment);
                $assignment->attachment = $newAttachment;
            }
            $assignment->save();

            if($request->attachment != NULL){
            $attachment->move('resource/attachment/', $newAttachment);
        }
        Alert::success('Berhasil', 'Tugas berhasil diedit');
            return redirect('course/'.$assignment->section->course->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignment = Assignment::findOrFail($id);
        $path = "resource/attachment/";
        File::delete($path.$assignment->attachment);
        Assignment::destroy($id);
        Alert::success('Berhasil', 'Tugas berhasil dihapu');
        return redirect()->back();
    }
}
