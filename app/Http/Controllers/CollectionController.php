<?php

namespace App\Http\Controllers;

use Alert;
use DB;
use File;
use App\Assignment;
use App\Collection;
use App\Enrollment;
use Illuminate\Http\Request;


class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            'assignment_id' => 'required',
            'attachment' => 'required',
        ]);
        
        $attachment = $request->attachment;
        
        $newAttachment = time() . '-' . $attachment->getClientOriginalName();
        $collection = new Collection;
        $collection->user_id = $request->user_id;
        $collection->assignment_id = $request->assignment_id;
        $collection->attachment = $newAttachment;
        $collection->save();

        $attachment->move('resource/collection/', $newAttachment);
        Alert::success('Berhasil', 'Tugas berhasil dikumpulkan');
        return redirect('mod/assignment/'.$request->assignment_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assignment = Assignment::find($id);
        $participant = Enrollment::with('user')->where('course_id', $assignment->section->course->id)->get()->sortBy('user.name');
        $key = 0;
        return view('collection.show', compact('assignment', 'participant', 'key'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->score != NULL) {
            $validatedData = $request->validate([
                'user_id' => 'required',
                'assignment_id' => 'required',
                'score' => 'required|min:0|max:100',
            ]);
            $collection = DB::table('collections')
            ->updateOrInsert(
                ['user_id' => $id, 'assignment_id' => $request->assignment_id],
                ['score' => $request->score]
            );
            

        } else {
            $validatedData = $request->validate([
                'assignment_id' => 'required',
                'attachment' => 'required',
            ]);
            
            $attachment = $request->attachment;
            $newAttachment = time() . '-' . $attachment->getClientOriginalName();
            
            $collection = Collection::where('user_id', $id)->where('assignment_id', $request->assignment_id)->first();
            File::delete('resource/collection/'.$collection->attachment);
            $collection = DB::table('collections')
            ->updateOrInsert(
                ['user_id' => $id, 'assignment_id' => $request->assignment_id],
                ['attachment' => $newAttachment]
            );
    
            $attachment->move('resource/collection/', $newAttachment);
            return redirect('mod/assignment/'.$request->assignment_id);
        }
        $validatedData = $request->validate([
            'assignment_id' => 'required',
            'attachment' => 'required',
        ]);
        
        $attachment = $request->attachment;
        $newAttachment = time() . '-' . $attachment->getClientOriginalName();
        
        $collection = Collection::where('user_id', $id)->where('assignment_id', $request->assignment_id)->first();
        File::delete('resource/collection/'.$collection->attachment);
        $collection->user_id = $id;
        $collection->assignment_id = $request->assignment_id;
        $collection->attachment = $newAttachment;
        $collection = DB::table('collections')
        ->updateOrInsert(
            ['user_id' => $id, 'assignment_id' => $request->assignment_id],
            ['attachment' => $newAttachment]
        );

        $attachment->move('resource/collection/', $newAttachment);
        Alert::success('Berhasil', 'Tugas berhasil dikumpulkan');
        return redirect('mod/assignment/'.$request->assignment_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
