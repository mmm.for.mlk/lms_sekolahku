<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceRecord extends Model
{
    protected $table = 'attendances_record';
    protected $fillable = ['attendances_form_id','user_id','date','status'];
    
    public function user()
    {
        return $this->belongsTo('App\UserModel');
    }

    public function attendanceForm()
    {
        return $this->belongsTo('App\AttendanceForm');
    }
}