# `FINAL PROJECT (Kelompok 24)`

## `Anggota Kelompok`

-   `Muhammad Ridwan Fauzi`
-   `Mulki Zulkarnaen Nurfalah`
-   `Yoga Setiawan`

## `Tema Project`

`Learning Management System`

## `ERD`

![ERD Design Learning Management System](keterangan%20kelompok/ERD-Design.png)

## `Link Video`

-   `Link video demo : `[`https://youtu.be/5pDzFqdfym4`](https://youtu.be/5pDzFqdfym4)
-   `Link deploy app : `[`http://tebakdiri.com`](http://tebakdiri.com)

## `Akun untuk akses`

`Anda dapat mendaftar akun baru yang nantinya menjadi seorang student atau menggunakan akun di bawah ini`

`Email : teacher@sekolahku.com`\
`Password : password`
