<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');


// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware('auth');


Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


Route::middleware(['auth'])->group(function () {
    //yoga
    Route::resource('profile','ProfileController');
    //section - bagian mulki
    Route::resource('course/{course_id}/section', 'SectionController')->only(['store', 'update', 'destroy']);
    Route::resource('course', 'CourseController')->except(['edit', 'create  ']);
    Route::resource('attendance-record', 'AttendanceRecordController')->only(['index', 'store', 'show', 'update','destroy']);
    Route::get('/attendance-recordpdf/{attendanceform_id}', 'AttendanceRecordController@printpdf');
    Route::get('/attendance-recordexcel/{attendanceform_id}', 'AttendanceRecordController@printexcel');
    Route::resource('course/{course_id}/enrollment', 'EnrollmentController')->only(['index', 'store', 'destroy']);
    Route::resource('mod/resource','ResourceController')->only(['store','edit','update', 'destroy']);
    Route::resource('mod/url','ExternalLinkController')->only(['store','edit','update', 'destroy']);
    Route::resource('mod/assignment','AssignmentController')->only(['store','show', 'edit','update', 'destroy']);
    Route::resource('mod/attendance-form','AttendanceFormController')->only(['store','show', 'edit','update', 'destroy']);
    Route::resource('collection','CollectionController')->only(['index', 'store','show','update', 'destroy']);
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::post('/dashboard', 'HomeController@register');
});


